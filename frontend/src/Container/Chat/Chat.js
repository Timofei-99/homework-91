import React, {useEffect, useRef, useState} from "react";
import {Button, Grid, makeStyles, TextField, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";

const useStyles = makeStyles({
    list: {
        listStyleType: "none",
        maxHeight: "450px",
        overflowY: "scroll",
        border: "1px solid black",
    },
    half: {
        minWidth: "45%",
    },
});

const Chat = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [message, setMessage] = useState("");
    const messages = useSelector((state) => state.chat.messages);
    const users = useSelector((state) => state.chat.users);
    const user = useSelector((state) => state.users.user);

    const ws = useRef(null);

    useEffect(() => {
        ws.current = new WebSocket("ws://localhost:8000/chat?token=" + user.token);

        ws.current.onmessage = (event) => {
            const decoded = JSON.parse(event.data);

            dispatch(decoded);
        };

        ws.current.onclose = () => {
            ws.current = null;

            setTimeout(() => {
                ws.current = new WebSocket("ws://localhost:8000/chat?token=" + user.token);
            }, 1000);
        };
    }, [user, dispatch]);

    const sendMessage = () => {
        ws.current.send(JSON.stringify({type: "CREATE_MESSAGE", message, user}));

        setMessage("");
    };

    return (
        <Grid container spacing={2} justifyContent="space-between" style={{flexWrap: "nowrap"}}>
            <Grid item container direction="column" className={classes.half}>
                <Typography variant="h6">Online User</Typography>
                <ul>
                    {users &&
                    users.map((user, index) => {
                        return (
                            <li key={index}>
                                {index + 1}. {user}
                            </li>
                        );
                    })}
                </ul>
            </Grid>
            <Grid item container direction="column" spacing={2}>
                <Grid item className={classes.half}>
                    <Typography variant="h6">Chat Room</Typography>
                    <ul className={classes.list}>
                        {messages.map((message, index) => (
                            <li key={index}>
                                <strong>{message.user.displayName}:</strong>
                                {message.text}
                            </li>
                        ))}
                    </ul>
                    <Grid item>
                        <TextField value={message} onChange={(e) => setMessage(e.target.value)} type="text"/>
                    </Grid>
                    <Grid item style={{margin: "10px 0"}}>
                        <Button variant="outlined" color="secondary" onClick={sendMessage}>
                            Send
                        </Button>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Chat;
