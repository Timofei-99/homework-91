import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Link as RouterLink} from "react-router-dom";
import {Avatar, Container, Grid, Link, makeStyles, Typography} from "@material-ui/core";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import ButtonWithProgress from "../../Component/ButtonWithProgress/ButtonWithProgress";
import FormElement from "../../Component/Form/FormElement";
import {registerUser} from "../../store/actions/userActions";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    header: {
        marginBottom: theme.spacing(2),
    },
}));

const Register = () => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const [user, setUser] = useState({
        username: "",
        password: "",
        displayName: "",
    });

    const loading = useSelector((state) => state.loading);
    const error = useSelector((state) => state.users.registerError);

    const inputChangeHandler = (e) => {
        const {name, value} = e.target;

        setUser((prev) => ({...prev, [name]: value}));
    };

    const submitFormHandler = (e) => {
        e.preventDefault();

        dispatch(registerUser({...user}));
    };

    const getFieldError = (fieldName) => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <Container component="section" maxWidth="xs">
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon/>
                </Avatar>
                <Typography component="h1" variant="h5" className={classes.header}>
                    Sign up
                </Typography>
                <Grid container spacing={1} direction="column" component="form" onSubmit={submitFormHandler}>
                    <FormElement
                        label="Username"
                        name="username"
                        value={user.username}
                        onChange={inputChangeHandler}
                        autoComplete="new-username"
                        type="text"
                        error={getFieldError("username")}
                        required
                    />
                    <FormElement
                        label="Password"
                        name="password"
                        value={user.password}
                        onChange={inputChangeHandler}
                        autoComplete="new-password"
                        type="password"
                        error={getFieldError("password")}
                        required
                    />
                    <FormElement
                        label="Display Name"
                        name="displayName"
                        value={user.displayName}
                        onChange={inputChangeHandler}
                        error={getFieldError("displayName")}
                        type="text"
                        required
                    />

                    <Grid item xs>
                        <ButtonWithProgress
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            loading={loading}
                            disabled={loading}
                        >
                            Sign Up
                        </ButtonWithProgress>
                    </Grid>
                    <Grid item container justifyContent="flex-end">
                        <Grid item>
                            <Link component={RouterLink} variant="body2" to="/login">
                                Already have an account? Sign in
                            </Link>
                        </Grid>
                    </Grid>
                </Grid>
            </div>
        </Container>
    );
};

export default Register;
