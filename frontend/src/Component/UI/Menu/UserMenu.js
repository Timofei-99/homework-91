import React from "react";
import {useDispatch} from "react-redux";
import {Button, Grid} from "@material-ui/core";
import {logoutUser} from "../../../store/actions/userActions";

const UserMenu = ({user}) => {
    const dispatch = useDispatch();

    return (
        <Grid container alignItems="center">
            <b>Hello, {user.displayName}!</b>
            <Button style={{color: "white"}} onClick={() => dispatch(logoutUser())}>
                Logout
            </Button>
        </Grid>
    );
};

export default UserMenu;
